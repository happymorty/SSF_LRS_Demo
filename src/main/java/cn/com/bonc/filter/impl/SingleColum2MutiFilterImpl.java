/**
 * FileName: DefaultfilterImpl
 * Author:   SAMSUNG-PC 孙中军
 * Date:     2019/2/14 12:40
 * Description: 不对数据内容处理，只是单纯的实现将单列数据转换为多列
 */
package cn.com.bonc.filter.impl;

import cn.com.bonc.filter.Filter;
import cn.com.bonc.util.ColumnsUtil;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;

import java.io.IOException;

public class SingleColum2MutiFilterImpl implements Filter {
    @Override
    public Dataset<Row> doFilter(Dataset<Row> rowDataset) {
        return ColumnsUtil.getInstance().getMultiColumnDataset(rowDataset,"value");
    }
}
