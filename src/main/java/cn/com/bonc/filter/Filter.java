/**
 * FileName: Filter
 * Author:   SAMSUNG-PC 孙中军
 * Date:     2019/1/17 10:42
 * Description:
 */
package cn.com.bonc.filter;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;

public interface Filter {
    Dataset<Row> doFilter(Dataset<Row> rowDataset);
}
